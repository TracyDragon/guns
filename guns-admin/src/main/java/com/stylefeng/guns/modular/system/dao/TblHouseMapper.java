package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.TblHouse;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author TracyLxy
 * @since 2018-10-28
 */
public interface TblHouseMapper extends BaseMapper<TblHouse> {

}
